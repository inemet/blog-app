import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/user';
import { UserService } from 'src/app/Services/user.service';
import Swal from 'sweetalert2';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required, Validators.min(3)])
  hide = true;

  matcher = new MyErrorStateMatcher();


  constructor(private service: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  register(enteredUsername: string, enteredEmail: string, enteredPassword: string): void {
    var object = new User(enteredUsername, enteredEmail, enteredPassword);
    this.service.registerUser(object).subscribe( (response) => {
      if (response != null) {
        Swal.fire({
          icon: 'success',
          title: 'Register',
          text: 'Register successful!',
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['login']);
          }
        })
      }
    },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Register',
          text: 'Check credentials!',
        })
      });
  }

}
