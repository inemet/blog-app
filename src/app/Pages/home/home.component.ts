import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/Models/post';
import { PostService } from 'src/app/Services/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  category: string = "";

  res: Post[] = [];
  data: [] = [];

  constructor(private service: PostService) { }

  ngOnInit(): void {
    this.service.getPosts().subscribe( (response: Post[]) => {
      this.res = response;
    })
  }

  onShowCategory(newCategory: string): void{
    this.category = newCategory;
    this.service.getPostsByCategories(this.category).subscribe( (result: Post[]) => {
      this.res = result;
    })
  }

}
