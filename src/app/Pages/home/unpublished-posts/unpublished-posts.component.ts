import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlloPostIds, AllowPost, Post } from 'src/app/Models/post';
import { PostService } from 'src/app/Services/post.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-unpublished-posts',
  templateUrl: './unpublished-posts.component.html',
  styleUrls: ['./unpublished-posts.component.css']
})
export class UnpublishedPostsComponent implements OnInit {

  res: Post[] = [];
  postId: number | undefined;
  deletePostId: number | undefined;

  constructor(private service: PostService, private router: Router) { }

  ngOnInit(): void {
    this.service.getUnpublishedPosts().subscribe((response: Post[]) => {
      this.res = response;
    })
  }


  getPostId(post: Post): void {
    this.postId = post.id;
    var newObject = new AllowPost(true);
    var payload = new AlloPostIds(newObject);
    this.service.updatePost(payload, this.postId).subscribe((result) => {
      if (result !== null) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, allow it!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Success!',
              'Your file has been posted.',
              'success'
            )
            this.router.navigate(['home'])
          }
        })
      }
    });
  }

  deletePost(post: Post): void {
    this.deletePostId = post.id;
    this.service.deletePost(this.deletePostId).subscribe((result) => {
      if (result !== null) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deleted!',
              'Your post has been deleted.',
              'success'
            )
            this.router.navigate(['home']);
          }
        })
      }
    });
  }

}
