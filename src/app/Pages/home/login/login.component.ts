import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Login } from 'src/app/Models/login';
import { UserService } from 'src/app/Services/user.service';
import { Router } from '@angular/router';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required, Validators.min(3)])
  hide = true;

  matcher = new MyErrorStateMatcher();

  constructor(private service: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  login(inputEmail: string, inputPassword: string): void {
    var object = new Login(inputEmail, inputPassword);
    this.service.loginUser(object).subscribe((response: any) => {
      if (response != null) {
        Swal.fire({
          icon: 'success',
          title: 'Login',
          text: 'Login successful!',
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['home']);
          }
        })
        localStorage.setItem('user', response.jwt);
        localStorage.setItem('userId', response.user.id);
      }
    },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Login',
          text: 'Wrong username or password!',
        })
      })

  }

}
