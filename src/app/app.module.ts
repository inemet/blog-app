import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBadgeModule } from '@angular/material/badge';
import { HeaderComponent } from './Components/header/header.component';
import { HomeComponent } from './Pages/home/home.component';
import { FilterComponent } from './Components/filter/filter.component';
import { MatSelectModule } from '@angular/material/select';
import { LoginComponent } from './Pages/home/login/login.component';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './Pages/home/register/register.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HeaderDialogComponent } from './Components/header-dialog/header-dialog.component';
import { UnpublishedPostsComponent } from './Pages/home/unpublished-posts/unpublished-posts.component';
import { CategoriesComponent } from './Components/categories/categories.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FilterComponent,
    LoginComponent,
    RegisterComponent,
    HeaderDialogComponent,
    UnpublishedPostsComponent,
    CategoriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatGridListModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatExpansionModule,
    MatListModule,
    MatToolbarModule,
    MatBadgeModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    EditorModule,
    MatProgressSpinnerModule,
    MatChipsModule
  ],
  providers: [{
    provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js'
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
