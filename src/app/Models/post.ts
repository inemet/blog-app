export interface Root {
  data: Post[]
  meta: Meta
}

export interface Post {
  id: number
  attributes: Attributes
}

export interface Attributes {
  Title: string
  Description: string
  Published: boolean
  createdAt: string
  updatedAt: string
  publishedAt: string
  Image: Image
  Document: Document
  categories: Categories
}

export interface Image {
  data: Data
}

export interface Data {
  id: number
  attributes: Attributes2
}

export interface Attributes2 {
  name: string
  alternativeText: any
  caption: any
  width: number
  height: number
  formats: Formats
  hash: string
  ext: string
  mime: string
  size: number
  url: string
  previewUrl: any
  provider: string
  provider_metadata: any
  createdAt: string
  updatedAt: string
}

export interface Formats {
  thumbnail: Thumbnail
}

export interface Thumbnail {
  name: string
  hash: string
  ext: string
  mime: string
  path: any
  width: number
  height: number
  size: number
  url: string
}

export interface Document {
  data: any
}

export interface Categories {
  data: Daum2[]
}

export interface Daum2 {
  id: number
  attributes: Attributes3
}

export interface Attributes3 {
  Name: string
  createdAt: string
  updatedAt: string
  publishedAt: string
}

export interface Meta {
  pagination: Pagination
}

export interface Pagination {
  page: number
  pageSize: number
  pageCount: number
  total: number
}

export interface PostCategories {
  id: number
  attributes: CategoriesForDisplay
}

export interface CategoriesForDisplay {
  Name: string
  createdAt: string
  updatedAt: string
  publishedAt: string
}


export class AddPost {
  Title: string;
  Description: string;

  constructor(title: string, description: string) {
    this.Title = title;
    this.Description = description;
  }
}

export class CategoryIdsPost{
  id: number;

  constructor(Id: number) {
    this.id = Id;
  }
}

export class CategoryIds {
  Title: string
  Description: string
  categories: CategoryIdsPost[]

  constructor(title: string, description: string, Categories: CategoryIdsPost[]) {
    this.Title = title;
    this.Description = description;
    this.categories = Categories;
  }
}

export class Payload {
  data: CategoryIds

  constructor(Data: CategoryIds) {
    this.data = Data;
  }
}


export class AllowPost {
  Published: boolean

  constructor(published: boolean) {
    this.Published = published;
  }
}

export class AlloPostIds{
  data: AllowPost

  constructor(Data: AllowPost) {
    this.data = Data;
  }
}

