import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { AddPost } from '../Models/add-post';
import { Post, PostCategories } from '../Models/post';

const API_URL = 'http://localhost:1337/api';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  data = localStorage.getItem('user');

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.get<Post[]>(`${API_URL}/posts?filters[Published][$eq]=true&populate=*`, { headers: headers }).pipe(map((res: any) => {
      return res.data;
    }));
  }

  getUnpublishedPosts(): Observable<Post[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.get<Post[]>(`${API_URL}/posts?filters[Published][$eq]=false&populate=*`, { headers: headers }).pipe(map((res: any) => {
      return res.data;
    }));
  }

  getCategories(): Observable<PostCategories[]> {
    return this.http.get<PostCategories[]>(`${API_URL}/categories`).pipe(map((res: any) => {
      return res.data;
    }));
  }

  AddPost(post: AddPost): Observable<AddPost[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.post<AddPost[]>(`${API_URL}/posts`, post, { headers: headers });
  }

  updatePost(post: any, id: number): Observable<Post[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.put<Post[]>(`${API_URL}/posts/${id}`, post, { headers: headers });
  }

  deletePost(id: number): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.delete<any>(`${API_URL}/posts/${id}`, { headers: headers });
  }

  getPostsByCategories(category: string): Observable<Post[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.get<Post[]>(`${API_URL}/posts?filters[categories][Name][$eq]=${category}&filters[Published][$eq]=true&populate=*`, { headers: headers }).pipe(map((res: any) => {
      return res.data;
    }));
  }
}
