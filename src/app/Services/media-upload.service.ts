import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MediaUploadService {

  data = localStorage.getItem('user');

  constructor(private http: HttpClient) { }

  upload(file: FormData): Observable<FormData>{
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.data}`
    })
    return this.http.post<FormData>('http://localhost:1337/api/upload', file , {headers: headers})
  }
}
