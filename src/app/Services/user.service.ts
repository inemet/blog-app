import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Login } from '../Models/login';
import { User } from '../Models/user';

const API_URL = 'http://localhost:1337/api/auth/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private router: Router) { }

  registerUser(user: User): Observable<User>{
    return this.http.post<User>(`${API_URL}local/register`,user);
  }

  loginUser(login: Login): Observable<Login>{
    return this.http.post<Login>(`${API_URL}local`,login);
  }

  LoggedIn(): boolean{
    if(localStorage.getItem('user') !== null){
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }

}
