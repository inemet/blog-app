import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { PostCategories } from '../Models/post';

const API_URL = 'http://localhost:1337/api/categories'

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }


  getCategories(): Observable<PostCategories[]> {
    return this.http.get<PostCategories[]>(`${API_URL}`).pipe(map((res: any) => {
      return res.data;
    }));
  }
}
