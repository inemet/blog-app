import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryIds, CategoryIdsPost, Daum2, Payload } from 'src/app/Models/post';
import { MediaUploadService } from 'src/app/Services/media-upload.service';
import { PostService } from 'src/app/Services/post.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header-dialog',
  templateUrl: './header-dialog.component.html',
  styleUrls: ['./header-dialog.component.css']
})
export class HeaderDialogComponent implements OnInit {

  html = '';

  editorConfig = {
    base_url: 'tinymce',
    suffix: '.min',
    plugins: 'lists link image table wordcount'
  };

  itemsFromAPI: Daum2[] = [];

  file: File | undefined;
  image: File | undefined;
  data: any;
  categoryIdSingle: string = "";
  categoryIdMultiple: CategoryIdsPost[] = [];

  constructor(private service: PostService, private http: HttpClient, private router: Router, private media: MediaUploadService) { }

  ngOnInit(): void {
    this.service.getCategories().subscribe((items) => {
      this.itemsFromAPI = items
    })
    this.data = localStorage.getItem('user');
  }

  getCategotyId(categoryId: Daum2): void {
    var newCategoryIds = new CategoryIdsPost(categoryId.id)
    this.categoryIdMultiple.push(newCategoryIds);
  }

  getFile(event: any) {
    this.file = event.target.files[0];
  }

  getImage(event: any) {
    this.image = event.target.files[0];
  }

  upload(): void {
    if (this.image) {
      const formData = new FormData();
      formData.append("files", this.image);
      this.media.upload(formData).subscribe((response: any) => {
        console.log(response);
      })
    } else if (this.file){
      const formData = new FormData();
      formData.append("files", this.file);
      this.media.upload(formData).subscribe((response: any) => {
        console.log(response);
      })
    }
  }

  getInput(inputTitle: string) {
    var newObject = new CategoryIds(inputTitle, this.html, this.categoryIdMultiple);
    var payload = new Payload(newObject);
    this.service.AddPost(payload).subscribe((result: any) => {
      if (result != null) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Your post has been saved',
          showConfirmButton: false,
          timer: 1500
        }).then((refId) => {
          if (this.image) {
            const formData = new FormData();
            formData.append("files", this.image);
            formData.append("refId", result.data.id);
            formData.append("ref", "api::post.post");
            formData.append("field", "Image");
            this.media.upload(formData).subscribe();
          }
        }).then( (refId) => {
          if (this.file) {
            const formData2 = new FormData();
            formData2.append("files", this.file);
            formData2.append("refId", result.data.id);
            formData2.append("ref", "api::post.post");
            formData2.append("field", "Document");
            this.media.upload(formData2).subscribe();
          }
        })
        this.router.navigate(['home']);
      }
    },
      (error) => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'Create post',
          text: 'Please enter post title and description!',
        })
      }
    );
  }
}
