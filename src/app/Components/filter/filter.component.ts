import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PostCategories } from 'src/app/Models/post';
import { PostService } from 'src/app/Services/post.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  @Output() showCategory = new EventEmitter<string>();

  itemsFromAPI : PostCategories[] = [];

  constructor(private service: PostService) { }

  ngOnInit(): void {
    this.service.getCategories().subscribe( (items) => {
      this.itemsFromAPI = items
    })
  }

  OnClick(category: string): void{
    this.showCategory.emit(category);
  }

}
