import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { HeaderDialogComponent } from '../header-dialog/header-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  data: any;
  number: number = 0;

  constructor(public dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {

  }

  LoggedIn() {
    return localStorage.getItem('user');
  }

  isAdmin(): boolean {
    if ((localStorage.getItem('userId')) == '4' || (localStorage.getItem('userId')) == '6') {
      return true;
    } else {
      return false;
    }
  }

  Logout() {
    localStorage.clear();
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Logout successful',
      showConfirmButton: false,
      timer: 1500
    })
    this.router.navigate(['login']);
  }

  openDialog() {
    this.dialog.open(HeaderDialogComponent);
  }

}
