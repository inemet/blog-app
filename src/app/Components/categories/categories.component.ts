import { Component, OnInit } from '@angular/core';
import { PostCategories } from 'src/app/Models/post';
import { CategoryService } from 'src/app/Services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: PostCategories[] = [];

  spinnerSpinning : boolean = true;

  constructor(private service: CategoryService) { }

  ngOnInit(): void {
    this.service.getCategories().subscribe((result) => {
      this.categories = result;
      this.spinnerSpinning = false;
    })
  }

}
